USE ApiAspCoreTest
GO 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_ObtenerNoticias
(
	@NoticiaId INT = NULL
)
AS
BEGIN
	
	SELECT
		*
	FROM Noticias
	WHERE NoticiaID = ISNULL(@NoticiaId, NoticiaID)

END
GO
