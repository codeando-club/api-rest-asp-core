﻿using API_Rest.Entities;
using API_Rest.Infraestructure.Abstract;
using API_Rest.Models.Autores.Output;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Infraestructure.Concret
{
    public class EFAutoresRepository : IAutoresRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBNoticiasContext _Context;
        #endregion

        public EFAutoresRepository(DBNoticiasContext dbNoticiasContext)
        {
            _Context = dbNoticiasContext;
        }

        public List<AutoresModel> ObtenerAsync()
        {
            List<AutoresModel> _List = new List<AutoresModel>();

            try
            {
                // En caso de no esperar una respuesta
                // _Context.Database.ExecuteSqlCommand("sp_ObtenerAutores");

                // Esperando una respuestas
                _List = _Context.Autor.FromSql("sp_ObtenerAutores").ToList();
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _List;
        }
    }
}
