﻿using API_Rest.Entities;
using API_Rest.Infraestructure.Abstract;
using API_Rest.Models.Noticias.Output;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Infraestructure.Concret
{
    public class EFNoticiasRepository : INoticiasRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBNoticiasContext _Context;
        #endregion

        public EFNoticiasRepository(DBNoticiasContext dbNoticiasContext)
        {
            _Context = dbNoticiasContext;
        }

        public bool AgregarAsync(NoticiasModel model)
        {
            bool _Resp = false;

            try
            {
                _Context.Noticia.Add(model);
                _Context.SaveChanges();

                _Resp = true;
            }
            catch(Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public bool ActualizarAsync(NoticiasModel model)
        {
            bool _Resp = false;

            try
            {
                // Obtenemos la noticia
                var _Noticia = _Context.Noticia.Where(x => x.NoticiaId == model.NoticiaId).FirstOrDefault();

                // Modificamos los campos
                _Noticia.Titulo = model.Titulo;
                _Noticia.Descripcion = model.Descripcion;
                _Noticia.Contenido = model.Contenido;
                _Noticia.Fecha = model.Fecha;
                _Noticia.AutorId = model.AutorId;
                
                // Guardamos
                _Context.SaveChanges();

                _Resp = true;
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public List<NoticiasModel> ObtenerAsync()
        {
            List<NoticiasModel> _Resp = new List<NoticiasModel>();

            try
            {
                _Resp = _Context.Noticia.Include(x => x.Autor).ToList();
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public List<NoticiasModel> ObtenerPorIdAsync(int NoticiaId)
        {
            List<NoticiasModel> _Resp = new List<NoticiasModel>();

            try
            {
                _Resp = _Context.Noticia.Include(x => x.Autor).Where(x => x.NoticiaId == NoticiaId).ToList();
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public List<NoticiasModel> ObtenerAsync(int NoticiaId)
        {
            List<NoticiasModel> _Resp = new List<NoticiasModel>();

            try
            {
                // Damos formato al query
                SqlParameter parameterNoticiaId = new SqlParameter("@NoticiaId", NoticiaId);

                // Ejecutamos el SP
                _Resp = _Context.Noticia.FromSql($"sp_ObtenerNoticias @NoticiaId", parameterNoticiaId).ToList();
                
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public bool EliminarAsync(int NoticiaId)
        {
            bool _Resp = false;

            try
            {
                // Obtenemos la noticia
                var _Noticia = _Context.Noticia.Where(x => x.NoticiaId == NoticiaId).FirstOrDefault();

                // Eliminamos
                _Context.Remove(_Noticia);
                _Context.SaveChanges();

                _Resp = true;
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }
    }
}
