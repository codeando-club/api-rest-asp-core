﻿using API_Rest.Models.Noticias.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Infraestructure.Abstract
{
    public interface INoticiasRepository
    {
        bool AgregarAsync(NoticiasModel model);

        List<NoticiasModel> ObtenerAsync();

        List<NoticiasModel> ObtenerPorIdAsync(int NoticiaId);

        bool ActualizarAsync(NoticiasModel model);

        bool EliminarAsync(int NoticiaId);
    }
}
