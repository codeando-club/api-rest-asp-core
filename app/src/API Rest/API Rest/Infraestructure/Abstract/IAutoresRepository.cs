﻿using API_Rest.Models.Autores.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Infraestructure.Abstract
{
    public interface IAutoresRepository
    {
        List<AutoresModel> ObtenerAsync();
    }
}
