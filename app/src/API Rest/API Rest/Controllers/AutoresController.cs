﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Rest.Infraestructure.Abstract;
using API_Rest.Models.Autores.Output;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API_Rest.Attributes;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoresController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IAutoresRepository _IAutoresRepository;
        #endregion

        public AutoresController(
            IAutoresRepository iAutoresRepository
        )
        {
            _IAutoresRepository = iAutoresRepository;
        }

        /// <summary>
        /// Obtiene los autores
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>Una lista de objetos AutoresModel</returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>  
        /// <response code="401">Unauthorized request</response>  
        /// <response code="500">Internal server error</response>  
        [HttpGet]
        [Route("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<AutoresModel>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        // [AuthorizationHeader]
        public ActionResult Get()
        {
            try
            {
                var _Resp = _IAutoresRepository.ObtenerAsync();

                return Ok(_Resp);
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                return BadRequest();
            }
        }
    }
}
