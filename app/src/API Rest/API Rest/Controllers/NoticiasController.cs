﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Rest.Infraestructure.Abstract;
using API_Rest.Models.Noticias.Output;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoticiasController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly INoticiasRepository _INoticiasRepository;
        #endregion

        public NoticiasController(
            INoticiasRepository iNoticiasRepository
        )
        {
            _INoticiasRepository = iNoticiasRepository;
        }

        // GET api/noticias
        [HttpGet]
        [Route("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<NoticiasModel>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        // [Authorize]
        [Authorize("read:messages")]
        public ActionResult Get()
        {
            try
            {
                var _Resp = _INoticiasRepository.ObtenerAsync();

                return Ok(_Resp);
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                return BadRequest();
            }
        }

        // GET api/noticias
        [HttpPost]
        [Route("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        public ActionResult Post([FromBody]NoticiasModel model)
        {
            try
            {
                if (_INoticiasRepository.AgregarAsync(model))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                return BadRequest();
            }
        }

        [HttpPut]
        [Route("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        public ActionResult Put([FromBody]NoticiasModel model)
        {
            try
            {
                if (_INoticiasRepository.ActualizarAsync(model))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        public ActionResult Delete(int NoticiaId)
        {
            try
            {
                if (_INoticiasRepository.EliminarAsync(NoticiaId))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                return BadRequest();
            }
        }
    }
}
