﻿using API_Rest.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Attributes
{
    public class AuthorizationHeaderAttribute : TypeFilterAttribute
    {
        public AuthorizationHeaderAttribute() : base(typeof(AuthorizationHeaderFilter))
        {
        }
    }
}
