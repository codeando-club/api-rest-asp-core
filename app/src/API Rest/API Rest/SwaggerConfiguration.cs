﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "API Rest v1 (Description)";

        public const string EndpointUrl = "/swagger/v1/swagger.json";

        public const string ContactName = "Paulo Andrade";

        public const string ContactUrl = "http://codeando.club";

        public const string DocNameV1 = "v1";

        public const string DocInfoTitle = "API Rest";

        public const string DocInfoVersion = "v1";

        public const string DocInfoDescription = "API Rest - Sample Web API in ASP.NET Core 2";
    }
}
