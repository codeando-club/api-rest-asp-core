﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Filters
{
    public class AuthorizationHeaderFilter : IResultFilter
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        #endregion

        public void OnResultExecuting(ResultExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add(
                "Authorization",
                "header basic auth");
        }

        public void OnResultExecuted(ResultExecutedContext context)
        {
            try
            {
                
            }
            catch(Exception e)
            {
                _Logger.Error(e);
            }
        }
    }
}
