﻿using API_Rest.Models.Autores.Output;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Models.Noticias.Output
{
    public class NoticiasModel
    {
        public int NoticiaId { get; set; }
        public string Descripcion { get; set; }
        public string Titulo { get; set; }
        public string Contenido { get; set; }
        public DateTime Fecha { get; set; }
        public int AutorId { get; set; }
        public AutoresModel Autor { get; set; }

        // Generamos el mapeo para el modelo Noticias
        public class Mapeo
        {
            public Mapeo(EntityTypeBuilder<NoticiasModel> MapeoNoticias)
            {
                // Asignamos el nombre de la tabla
                MapeoNoticias.ToTable("Noticias");

                // Asignamos la primary key
                MapeoNoticias.HasKey(x => x.NoticiaId);

                // En caso de que el nombre de la columna sea diferente en la base de datos
                // Asignamos el nombre de la columna
                MapeoNoticias.Property(x => x.Titulo).HasColumnName("Titulo");
                MapeoNoticias.Property(x => x.Descripcion).HasColumnName("Descripcion");
                MapeoNoticias.Property(x => x.Contenido).HasColumnName("Contenido");
                MapeoNoticias.Property(x => x.Fecha).HasColumnName("Fecha");
                MapeoNoticias.Property(x => x.AutorId).HasColumnName("AutorId");

                // Especificamos la relación
                MapeoNoticias.HasOne(x => x.Autor);
            }
        }
    }
}
