﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Models.Autores.Output
{
    public class AutoresModel
    {
        public int AutorId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        // Generamos el mapeo para el modelo Noticias
        public class Mapeo
        {
            public Mapeo(EntityTypeBuilder<AutoresModel> MapeoAutores)
            {
                // Asignamos el nombre de la tabla
                MapeoAutores.ToTable("Autores");

                // Asignamos la primary key
                MapeoAutores.HasKey(x => x.AutorId);

                // En caso de que el nombre de la columna sea diferente en la base de datos
                // Asignamos el nombre de la columna
                MapeoAutores.Property(x => x.Nombre).HasColumnName("Nombre");
                MapeoAutores.Property(x => x.Apellido).HasColumnName("Apellido");
            }
        }
    }
}
