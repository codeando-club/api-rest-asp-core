﻿using API_Rest.Models.Autores.Output;
using API_Rest.Models.Noticias.Output;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Entities
{
    public class DBNoticiasContext : DbContext
    {
        public DBNoticiasContext(DbContextOptions Options) : base(Options)
        {

        }

        // Declaramos las tablas
        public DbSet<NoticiasModel> Noticia { get; set; }
        public DbSet<AutoresModel> Autor { get; set; }

        // 
        protected override void OnModelCreating(ModelBuilder Model)
        {
            new NoticiasModel.Mapeo(Model.Entity<NoticiasModel>());
            new AutoresModel.Mapeo(Model.Entity<AutoresModel>());
        }
    }
}
