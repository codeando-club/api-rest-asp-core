﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using API_Rest.Attributes;
using API_Rest.Entities;
using API_Rest.Handlers;
using API_Rest.Helpers;
using API_Rest.Infraestructure.Abstract;
using API_Rest.Infraestructure.Concret;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace API_Rest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Agregamos autenticación basica
            services.AddAuthentication(options => options.AddScheme("BasicAuthentication", o => o.HandlerType = typeof(BasicAuthenticationHandler)));

            // AUTH0
            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:ApiIdentifier"];
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = ClaimTypes.NameIdentifier
                };
            });

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                // configuración inicial de swagger
                c.SwaggerDoc(SwaggerConfiguration.DocNameV1,
                            new Microsoft.OpenApi.Models.OpenApiInfo
                            {
                                Title = SwaggerConfiguration.DocInfoTitle,
                                Version = SwaggerConfiguration.DocInfoVersion,
                                Description = SwaggerConfiguration.DocInfoDescription,
                                Contact = new Microsoft.OpenApi.Models.OpenApiContact
                                {
                                    Name = SwaggerConfiguration.ContactName,
                                    Url = new Uri(SwaggerConfiguration.ContactUrl)
                                }
                                //License = new OpenApiLicense
                                //{
                                //    Name = "Use under LICX",
                                //    Url = new Uri("https://example.com/license"),
                                //}
                            }
                );

                // Configuración de seguridad para swagger
                //c.AddSecurityDefinition("basic", new OpenApiSecurityScheme
                //{
                //    Name = "Authorization",
                //    Type = SecuritySchemeType.Http,
                //    Scheme = "basic",
                //    In = ParameterLocation.Header,
                //    Description = "Basic Authorization header using the Bearer scheme."
                //});
                //c.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    {
                //        new OpenApiSecurityScheme
                //        {
                //            Reference = new OpenApiReference
                //            {
                //                Type = ReferenceType.SecurityScheme,
                //                Id = "basic"
                //            }
                //        },
                //        new string[] {}
                //    }
                //});

                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    In = ParameterLocation.Header,
                    Description = "Please enter into field the word 'Bearer' following by space and JWT."
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "bearer"
                            }
                        },
                        new string[] {}
                    }
                });

                // Agregamos cabeceras personalizadas
                c.OperationFilter<CustomHeaderSwaggerAttribute>();

                // documentación XML
                // var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                // var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // c.IncludeXmlComments(xmlPath);
            });

            // Configuramos nuestro DbContext
            services.AddDbContext<DBNoticiasContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Noticias")));

            // Configuramos la validacion de ambitos (AUTH0 scopes)
            services.AddAuthorization(options =>
            {
                options.AddPolicy("read:messages", policy => policy.Requirements.Add(new HasScopeHelper("read:messages", domain)));
            });

            // Registramos el ambito authorization handler (AUTH0 scopes)
            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            // Agregamos las interfaces
            services.AddTransient<INoticiasRepository, EFNoticiasRepository>();
            services.AddTransient<IAutoresRepository, EFAutoresRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory //  Agregamos el factory de log4net
        )
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
                // c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Agregamos la autenticación basica
            app.UseAuthentication();

            // Respuestas personalizadas para peticiones no autorizadas
            app.UseStatusCodePages(async context =>
            {
                if (context.HttpContext.Request.Path.StartsWithSegments("/api") &&
                   (context.HttpContext.Response.StatusCode == 401 ||
                    context.HttpContext.Response.StatusCode == 403))
                {
                    await context.HttpContext.Response.WriteAsync("Unauthorized request");
                }
            });

            // app.UseHttpsRedirection();
            app.UseMvc();

            // Registramnos el middleware de log4net (Registro de sistema)
            loggerFactory.AddLog4Net();
        }
    }
}
