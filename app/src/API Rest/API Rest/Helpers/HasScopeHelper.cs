﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Rest.Helpers
{
    public class HasScopeHelper : IAuthorizationRequirement
    {
        #region Propiedades
        public string Issuer { get; }
        public string Scope { get; }
        #endregion

        public HasScopeHelper(string scope, string issuer)
        {
            Scope = scope ?? throw new ArgumentNullException(nameof(scope));
            Issuer = issuer ?? throw new ArgumentNullException(nameof(issuer));
        }
    }
}
